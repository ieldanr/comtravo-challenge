'use strict';

describe('Service: googleQPX', function () {

  // load the service's module
  beforeEach(module('comtravoChallenge2App'));

  // instantiate service
  var googleQPX;
  beforeEach(inject(function (_googleQPX_) {
    googleQPX = _googleQPX_;
  }));

  it('should do something', function () {
    expect(!!googleQPX).toBe(true);
  });

});
