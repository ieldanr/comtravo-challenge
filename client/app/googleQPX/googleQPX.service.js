'use strict';

angular.module('comtravoChallenge2App')
  .service('googleQPX', function ($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    //
    var url = 'https://www.googleapis.com/qpxExpress/v1/trips/search';
    // Parameters for request
    var origin = 'BOS';
    var destination = 'LAX';
    var date = '2016-03-10';
    var adultCount = 2;
    var maxPrice = 'USD1000';
    var solutionsCount = 20;

    // Flights
    var flightDetails = {};

    var buildParams = function() {
      return '{ "request": { "slice": [ { "origin": "' + origin + '",' +
          '"destination": "'+ destination + '",' +
          '"date": "' + date + '"}], "passengers": {' +
          '"adultCount": ' + adultCount +
          '}, "solutions": ' + solutionsCount + ',' +
          '"maxPrice": "' + maxPrice + '"} }';

    };

    this.getFlightDetails = function(){
      return flightDetails;
    };

    this.request = function() {
      var request = $http({
        method: 'post',
        url: url,
        params: { 'key': 'AIzaSyAelY3ODtSIZ0HrY9liozxR3psia28ikyQ' },
        data: buildParams(),
      });

      return request;
    };

    this.setOrigin = function(newOrigin) {
      origin = newOrigin;
    };

    this.getOrigin = function() {
      return origin;
    };

    this.setDestination = function(newDestination) {
      destination = newDestination;
    };

    this.getDestination = function() {
      return destination;
    };

    this.setDate = function(newDate) {
      date = newDate;
    };

    this.getDate = function() {
      return date;
    };

    this.setAdultCount = function(newAdultCount) {
      adultCount = newAdultCount;
    };

    this.getAdultCount = function() {
      return adultCount;
    };

    this.setMaxPrice = function(newMaxPrice) {
      maxPrice = newMaxPrice;
    };

    this.getMaxPrice = function() {
      return maxPrice;
    };
  });
