'use strict';

(function() {

class MainController {

  constructor($http, googleQPX) {

    // Google service
    this.googleQPX = googleQPX;

    // alerts and loading state
    this.alert = '';
    this.loading = false;

    //search Parameters
    this.origin = 'BOS';
    this.destination = 'LAX';
    this.date = new Date();
    this.numPassengers = 2;
    this.maxAmount = 'USD1000';

    // trip results
    this.trips = [];



    this.searchFlights = function() {
      this.loading = true;
      this.alert = '';

      //date format
      var dd = this.date.getDate();
      var mm = this.date.getMonth() + 1;
      var yyyy = this.date.getFullYear();

      if(dd < 10){
        dd = '0' + dd;
      }

      if(mm < 10){
        mm = '0' + mm;
      }

      var dateSearch = yyyy + '-' + mm + '-' + dd;

      //send params to service
      this.googleQPX.setOrigin(this.origin);
      this.googleQPX.setDestination(this.destination);
      this.googleQPX.setAdultCount(this.numPassengers);
      this.googleQPX.setDate(dateSearch);
      this.googleQPX.setMaxPrice(this.maxAmount);

      //send request
      this.googleQPX.request().success(response => {
        var newTrips = [];
        var trips = response.trips;
        for(var index in trips.tripOption){
          var newTrip = {};
          var trip = trips.tripOption[index];
          console.log(trip);
          newTrip.saleTotal = trip.saleTotal;
          var segments = trip.slice[0].segment;
          var initialLeg = segments[0].leg[0];
          var LastSegmentLegs = segments[segments.length - 1].leg;
          var lastLeg = LastSegmentLegs[LastSegmentLegs.length - 1];
          newTrip.departureTime = initialLeg.departureTime;
          newTrip.arrivalTime = lastLeg.arrivalTime;

          var stops = 0;
          var newLegs = [];
          for(var segIndex in segments){
            var segment = segments[segIndex];
            stops += segment.leg.length;
            for(var legIndex in segment.leg){
              var leg = segment.leg[legIndex];
              var newLeg = {};
              newLeg.carrier = segment.flight.carrier;
              newLeg.flightNumber = segment.flight.number;
              newLeg.origin = leg.origin;
              newLeg.destination = leg.destination;
              newLeg.departureTime = leg.departureTime;
              newLeg.arrivalTime = leg.arrivalTime;
              newLegs.push(newLeg);
            }
          }

          newTrip.stopCount = stops - 1;
          newTrip.legs = newLegs;
          newTrip.isCollapsed = true;
          newTrips.push(newTrip);
        }
        this.loading = false;
        this.trips = newTrips;
      }).error( response => {
        console.log(response);
        this.trips = [];
        this.loading = false;
        this.alert = response.error.message;
      });
    };
  }
}

angular.module('comtravoChallenge2App')
  .controller('MainController', MainController);

})();
